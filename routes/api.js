module.exports = function (app) {
  var api = app.controllers.api;

  app.get('/api/data', api.get);
  app.post('/api/data', api.save);
  app.delete('/api/data/wipe', api.wipe);
};
