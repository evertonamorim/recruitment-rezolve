'use strict';

var single_connection
        , db = {
          development: {
            uri: 'mongodb://localhost/rezolve_dev',
            params: {
              username: '',
              password: ''
            }
          },
          test: {
            uri: 'mongodb://localhost/rezolve_test',
            params: {
              username: '',
              password: ''
            }
          }
        }
;

module.exports = function (mongoose) {
  var uri = db[process.env.NODE_ENV || 'development'].uri;
  var params = db[process.env.NODE_ENV || 'development'].params;

  if (!single_connection) {
    mongoose.Promise = global.Promise;
    single_connection = mongoose.connect(uri, params);
  }

  return single_connection;
};
