'use strict';

/**
 * @namespace controllers
 * 
 * App API.
 * @module controllers/api
 * 
 * Parâmetros recebidos via post.
 * @var {Object} req.body
 * 
 * * Parâmetros capturados da url.
 * @var {Object} req.params
 */
module.exports = function (app) {
  var Deputado = app.models.deputado;
  
  var ApiController = {
    /**
      * @function get
      *
      * @example 
      *   app.controllers.api.get(req, res);
      *
      * @description Responde as requisições à rota '/api/data' feitas via GET. <br/>
      *              [GET] /api/data
      *  
      * @param   {Request}  req - HTTP Request 
      * @param   {Response} res - HTTP Response
      * @returns {JSON}         - Objeto JSON com todos os deputados
      */
    get: function(req, res) {
      Deputado.find({})
        .then(function(result) {
          res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(412).json({response: err.message});
        });
    },
    /**
      * @function save
      *
      * @example 
      *   app.controllers.api.save(req, res);
      * @description Responde as requisições à rota '/api/data' feitas via POST e salva um deputados no banco de dados. <br/>
      *              [POST] /api/data
      *  
      * @param   {Request}  req - HTTP Request 
      * @param   {Response} res - HTTP Response
      * @returns {JSON}         - 200 para sucesso e 402 para erro
      */
    save: function(req, res) {
      Deputado.create(req.body.data)
        .then(function(){
          res.status(200).json({response: 'Data is saved'});
        })
        .catch(function(err){          
          res.status(412).json({response: err.message});
        });
    },
    /**
      * @function wipe
      *
      * @example 
      *   app.controllers.api.wipe(req, res);
      * @description Responde as requisições à rota '/api/data/wipe' feitas via DELETE e apaga todos os dados da collection `deputados`. <br/>
      *              [DELETE] /api/data/wipe
      *  
      * @param   {Request}  req - HTTP Request 
      * @param   {Response} res - HTTP Response
      * @returns {JSON}         - 200 para sucesso e 402 para erro
      */
    wipe: function(req, res) {
      Deputado.remove({})
        .then(function() {
          res.status(200).json({response: 'All data wiped'});
        })
        .catch(function(err) {
          res.status(412).json({response: err.message});
        }); 
    }
  };
  
  return ApiController;
};
