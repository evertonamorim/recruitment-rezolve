'use strict';

/**
 * @namespace controllers
 * 
 * App Index.
 * @module controllers/index
 * 
 * Parâmetros recebidos via post.
 * @var {Object} req.body
 * 
 * * Parâmetros capturados da url.
 * @var {Object} req.params
 */
module.exports = function (app) {
  var IndexController = {
    /**
      * @function index
      *
      * @example 
      *   app.controllers.index.index(req, res);
      *
      * @description Executada em resposta à rota '/'. <br/>
      *              [GET] /
      *  
      * @param   {Request}  req - HTTP Request 
      * @param   {Response} res - HTTP Response
      * @returns {RenderHTML}   - Rendeniza a página inicial
      */
    index: function (req, res) {
      res.render('index/index');
    },
    /** @function test
      * @example 
      *   app.controllers.index.test(req, res);
      * @description Esta é função é executada em resposta à rota /test. <br/>
      *              [GET] /test
      * @param   {Request}  req - HTTP Request 
      * @param   {Response} res - HTTP Response
      * @returns {JSON}         - Objeto JSON de teste
      */
    test: function (req, res) {
      res.status(200)
         .json({Response: 'Test ok!!'});
    }
  };

  return IndexController;
};
