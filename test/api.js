var app = require('../app'),
    request = require('supertest')(app),
    rand = require('randomstring')
;

describe('controller api', function () {
  it('deve retornar status 200 ao fazer GET em /api/data', function (done) {
    request.get('/api/data')
           .end(function (err, res) {
              res.status.should.eql(200);
              done();
            });
  });

  it('deve retornar status 200 ao fazer POST em /api/data e adicionar um deputado', function (done) {
    var random = rand.generate(7),
        deputado = {
          fullName: 'Deputado_Test_'+random,
          birthday: '01-01',
          party: 'TEST',
          phone: '(00) 0000-0000',
          address: {
            publicPlace: 'TEST',
            cabinet: '0',
            postalCode: '00000-000',
            city: 'TEST',
            uf: 'T1'
          },
          email: 'test'+random+'@test.com'
        };
    
    request.post('/api/data')
          .send({data: deputado} )
          .end(function (err, res) {
             res.status.should.eql(200);
             done();
          });
  });
  
  it('deve retornar status 200 ao fazer DELETE em /api/data/wipe', function (done) {
    request.delete('/api/data/wipe')
          .end(function (err, res) {
             res.status.should.eql(200);
             done();
          });
  });
});