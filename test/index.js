var app = require('../app'),
    request = require('supertest')(app)
;

describe('controller index', function () {
  it('deve retornar status 200 ao fazer GET em /', function (done) {
    request.get('/')
           .end(function (err, res) {
              res.status.should.eql(200);
              done();
            });
  });
  
  it('deve retornar status 200 ao fazer GET em /test', function (done) {
    request.get('/test')
           .end(function (err, res) {
              res.status.should.eql(200);
              done();
            });
  });
});