# DEPS #

Uma aplicação que busca informações de deputados, e salva-as no mongodb.

Os dados são servidos via API e o sincronismo é feito em paralelo.


### Ambiente

Para sistemas operacionais linux baseados no Debian há um arquivo chamado `install.sh` para facilitar o setup do ambiente.

A saber, estes são os programas e versões em que a aplicação foi construída:

~~~~
__NodeJS:__ 4.2.6
__MongoDB:__ 3.2.6 
__Gulp:__ 3.9.1
__Bower:__ 1.7.9
__NPM:__ 3.10.6
~~~~


### Instalação
    
* Clone o repositório: `git clone https://fsjunior@bitbucket.org/fsjunior/recruitment-rezolve.git`
* Acesse o diretório do projeto: `cd recruitment-rezolve`
* Instale as dependências do node: `npm install`
* Instale as dependências client-side: `bower install`
* Inicie o servidor: `gulp`
* Para executar testes: `npm test`

_Obs.:_ Por padrão o `gulp` gera a documentação, minifica, ofusca e une arquivos `.js` e `.css` e os move para a pasta public. 

Por último, inicia a aplicação. A documentação gerada fica em `~project/docs/gen/index.html`

_Obs².:_ Você também pode executar o arquivo `install.sh` para instalar os programas se seu SO for compatível, conforme comentado acima.


### Contato

[jr.fernandodasilva@gmail.com](mailto:jr.fernandodasilva@gmail.com)  
