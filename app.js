'use strict';

//dependencies
var express = require('express'),
    load = require('express-load'),
    bodyParser = require('body-parser'),
    error = require('./middlewares/error'),
    http = require('http'),
    path = require('path'),
    sync = require('./sync'),
    schedule = require('node-schedule');

//sync data and scheduling to sync data
sync.start();
schedule.scheduleJob({hour: 3, minute: 0}, function(){
  console.log(Date(), 'Daily sync at 03:00hs');
  sync.start();
});

//create express app
var app = express();

//setup the web server
var server = http.createServer(app);

//set view
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('json spaces', 4);

//middleware
app.use(require('compression')());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// loads
load('models')
  .then('controllers')
  .then('routes')
  .into(app);

// error files
app.use(error.notFound);
app.use(error.serverError);

//listen up
server.listen(3000, function () {
  console.log(Date(), 'Server is running!!');
});

module.exports = app;