$(function () {
  var succesGet = function (json) {
    var rows = '',
        background = {
          val: 0,
          class: '',
          aux: 'bg-silver' 
        };
    
    for (var i in json) {
      if(background.val) {
        var aux = background.class;
        background.val = 0;
        background.class = background.aux;
        background.aux = aux;
      }
      
      background.val++;
      rows += element(json[i], background.class);
    }
    
    $('#deputados').empty().append(rows);
    
    function element(infos, customClass) {
      return '<tr class="'+customClass+'">\n\
                <td>'+infos.fullName+'</td>\n\
                <td>'+infos.birthday+'</td>\n\
                <td>'+infos.party+'</td>\n\
                <td>'+infos.state+'</td>\n\
                <td>'+(infos.main ? 'Sim' : 'Não')+'</td>\n\
                <td>'+infos.legislatures+'</td>\n\
                <td><a href="mailto:'+infos.email+'?">'+infos.email+'</a></td>\n\
                <td class="text-center">\n\
                  <a class="btn btn-xs btn-default" data-toggle="collapse" data-target="#'+infos._id+'"><span class="glyphicon glyphicon-envelope text-primary"></span> </a>\n\
                </td>\n\
              </tr>\n\
              <tr class="'+customClass+'">\n\
                <td colspan="8" class="no-padding">\n\
                  <div class="accordian-body collapse" id="'+infos._id+'"> \n\
                    <address>\n\
                      <strong><small>'+infos.fullName+'</small></strong><br>\n\
                      '+infos.contact.address+'<br>\n\
                      Gabinete: '+infos.contact.cabinet+'<br>\n\
                      CEP: '+infos.contact.postalCode+', '+infos.contact.city+' / '+infos.contact.uf+'<br>\n\
                      <abbr title="Telefone">Tel:</abbr> '+infos.phone+'\n\
                    </address>\n\
                  </div> \n\
                </td>\n\
              </tr>';  
    }
  };

  $.ajax({
    url: '/api/data',
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    success: succesGet
  });
});
