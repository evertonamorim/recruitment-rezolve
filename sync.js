'use strict';

/**
 * @mixin sync
 * @description Faz a sincronização dos dados quando a aplicação é iniciada e <br/>
 *              agenda outra sincronização diaria (no app.js) às 3hs AM.
 */
var request = require('request'),
    jsdom = require('jsdom');

module.exports = {
  count: 1,
  target: 'deputado',
  /**
    * @description Função inicial que começa o fluxo de sincronismo.
    * @return {Callback} Apaga todos os dados da collection `deputados` e passa a função this.getInfos() de callback
    */
  start: function() {
    console.log(Date(), 'sync started');
    var params = {
      url: 'http://www2.camara.leg.br/deputados/pesquisa'
    };
    this.reset(params, this.getInfos);
  },
  /**
    * @description Reseta os parâmetros do sync
    * @param {Object} paramsNext - Parâmetros para após resetar a collection `deputados`
    * @param {Function} next - Callback para depois de resetar a collection `deputados`
    * @return {Callback} Apaga os dados da collection 'deputados' e faz uma requisição para pegar os deputados
    */
  reset: function(paramsNext, next) {
    this.count = 1;
    var parent = this,
        params = {
          url: 'http://localhost:3000/api/data/wipe', 
          method: 'DELETE'
        };
    
    this.requestSync(params, function(){
      parent.requestSync(paramsNext, next);
    });
  },
  /**
   * @description É chamada como callback do `this.reset` e traz os ids dos deputadas após um request
   * @param {Throw} err - Um objeto de erro
   * @param {Object} parent - Uma referência ao this (global) deste objeto
   * @param {Object} deputados - Um objeto com todos os deputados vindo do request anterior a essa função
   * @return {Callback} Faz uma chamada de request para cada um dos deputados resgatados anteriormente com delay de 300ms. 
   */
  getInfos: function(err, parent, deputados) {
    var url = 'http://www.camara.leg.br/internet/Deputado/dep_Detalhe.asp?id=';
    deputados = deputados.options;
    parent.count = deputados.length - 1;
    parent.target = 'content';
    
    for(var i = 1; i <= parent.count; i++) {
      var id = deputados[i].value.split('?')[1];
      parent.delay(i * 300, {url: url+id});
    }
  },
  /**
    * @description "Serializa" os envios de dados, adicionando um tempo de atraso passado (parâmetro) <br/>
    *               e consumido a API via POST `/api/data`. 
    * @param {Number} time - Tempo de atraso entre os requests
    * @param {Object} params - Objeto com a url de um deputado.
    * @return {Callback} Faz um request de um deputado.
    */
  delay: function(time, params) {
    var parent = this;
    
    setTimeout(function() { 
      parent.requestSync(params, parent.populate);
    }, time);
  },
  /**
    * @description Busca dados a partir de uma {url, [método], [form]} <br/>
    *              Faz a conversão de html pasa json utilizando jsdom <br/>
    * @param {Object} params - Parâmetros para a realização do request
    * @param {Function} next - Função de callback para ser executada após o callback
    * @return {Callback} Executa o parâmetro next()
    */
  requestSync: function(params, next) {
    var parent = this,
        opts = {
          url: params.url,
          method: params.method || 'GET',
          form: params.form || {},
          headers: {
            'User-Agent': 'Mozilla/5.0'
          }
        };
    
    request(opts, function(err, response, body) {
      if (!err && response.statusCode == 200) {
        if (params.method === 'POST' || params.method === 'DELETE')
          return next(body);
     
        jsdom.env(body, function (err, window) {
          parent.count--;
          next(null, parent, window[parent.target]);
          window.close();  
        });
      } else {
        console.log('Erro no request from url: ' + params.url, response.statusCode, err, body);
      }
        
    });
  },
  /**
    * @description Cria uma estrutura JSON com os dados básicos do deputado. <br/> 
    *              É chamada como callback de `this.getInfos -> this.delay`
    * @param {Throw} err - Um objeto de erro
    * @param {Object} parent - Uma referência ao this (global) deste objeto
    * @return {Callback} Chama o this.finaly() pasa salvar as informações no bando de dados.
    */
  populate: function(err, parent, infos) {
    var deputado = infos.getElementsByTagName('li'),
        addressQtd = infos.querySelectorAll('div.bloco').length,
        address = infos.querySelectorAll('div.bloco')[addressQtd-1].getElementsByTagName('li'),
        pcu = address[2].childNodes[0].textContent.trim().split(' - '),
        birth = deputado[1].childNodes[1].textContent.split('/'),
        psm = deputado[2].childNodes[1].textContent.split('/'),
        deputado = {
          fullName: deputado[0].childNodes[1].textContent.trim(),
          birthday: ('0'+birth[0].trim()).substr(-2) + '-' + ('0'+birth[1].trim()).substr(-2),
          party: psm[0].trim(),
          state: psm[1].trim(),
          legislatures: deputado[4].childNodes[1].textContent.trim(),
          main: psm[2].trim() === 'Titular' ? true : false,
          phone: deputado[3].childNodes[1].textContent.replace(' - ', '').trim(),
          contact: {
            address: address[0].childNodes[0].textContent,
            cabinet: address[1].childNodes[0].textContent.trim().replace('Gabinete: ', ''),
            postalCode: pcu[0].replace('CEP: ', ''),
            city: pcu[1],
            uf: pcu[2]
          },
          email: address[3].childNodes[0].textContent
        };
    
    parent.finaly(deputado);
  },
  /**
    * @description Salva os dados no banco de dados <br/>
    * @param {JSON} deputado - Um JSON com informações do deputado
    * @return {Callback} this.requestSync - Faz um request para salvar o deputado.
    */
  finaly: function(deputado) {
    // console.log('Save data from: ' + deputado.fullName);
    var parent = this;
    
    var params = {
        url: 'http://localhost:3000/api/data',
        method: 'POST',
        form: {data: deputado}
    };
        
    this.requestSync(params, function(result) {
      if (!parent.count)
        console.log(Date(), 'sync finished');
    });
  },  
  /*
   * @function compare
   *
   * @example 
   *   var obj = [
   *     {'name': 'Fernando'}, 
   *     {'name': 'Caio'}, 
   *     {'name': 'Norris'}
   *   ];
   *   
   *   objSorted = obj.sort(compare('name'));
   *
   * @description Ordena um objeto a partir de uma key passada por parâmetro.
   *  
   * @param   {String} propName - O nome da key a ser usada na ordenação
   * @returns {Object}          - Retorno o objeto ordenado pela key informada
   */
  compare: function (propName) {
    return function(a,b) {
        if (a[propName] < b[propName])
            return -1;
        if (a[propName] > b[propName])
            return 1;
        return 0;
    };
  }
};