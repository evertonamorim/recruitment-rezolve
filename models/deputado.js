'use strict';

var mongoose = require('mongoose'),
    db = require('../libs/db_connect')(mongoose),
    rand = require("randomstring");

module.exports = function (app) {
  var deputado = new mongoose.Schema({
    fullName: { type: String, unique: true, default: ''},
    birthday: { type: String, default: '01-01'},
    party: String,
    state: { type: String, default: 'DF'},
    legislatures: String,
    main: {type: Boolean, default: true},
    phone: String,
    contact: {
      address: String,
      cabinet: String,
      postalCode: String,
      city: String,
      uf: String
    },
    email: {type: String, unique: true, default: rand.generate(7)+'@default_random.com'}
  });

  return db.model('deputado', deputado);
};
