'use strict';

exports.notFound = function (req, res, next) {
  res.status(404);
  res.render('layouts/error-404');
};

exports.serverError = function (error, req, res, next) {
  res.status(500);
  res.render('layouts/error-500', {error: error});
};
