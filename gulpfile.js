var gulp = require('gulp'),
    concat = require('gulp-concat'),        // Concatenate the files
    jshint = require('gulp-jshint'),        // Test the js files
    uglify = require('gulp-uglify'),        // Uglify/minify the js
    uglifycss = require('gulp-uglifycss'),  // Uglify/minify the css
    shell = require('gulp-shell'),          // Exec shell comands
    jsdoc = require('gulp-jsdoc3')          // Generate documentation
;

var js  = [
      './src/vendor/jquery/dist/jquery.min.js',
      './src/vendor/bootstrap/dist/js/bootstrap.min.js',
      './src/js/index.js'
    ],
    jsDocFiles = [
      'README.md',
      './controllers/*.js', 
      './sync.js',
      './src/js/*.js'
    ],
    css = [
      './src/vendor/bootstrap/dist/css/bootstrap.min.css',
      './src/css/general-styles.css',
      './src/css/index.css'
    ],
    fonts = [
      './src/vendor/bootstrap/dist/fonts/*'
    ],
    defaultTasks = [
      'doc',
      'lint',  
      'minify-js', 
      'minify-css',
      'start-project'
    ];

gulp.task('lint', function() {
  gulp.src(js)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
    
});
 
gulp.task('doc', function (cb) {
  gulp.src(jsDocFiles, {read: false})
      .pipe(jsdoc(cb));
});
 
gulp.task('minify-js', function () {
  gulp.src(js)                         // Load files
    .pipe(concat('index.min.js'))      // Concatenate all files to only one
    .pipe(uglify())                    // Uglify file content
    .pipe(gulp.dest('./public/js/'));  // Folder to save file
});

gulp.task('minify-css', function () {
  gulp.src(css)                      
    .pipe(concat('index.min.css'))     
    .pipe(uglifycss({uglyComments: true}))                 
    .pipe(gulp.dest('./public/css/')); 
    
  gulp.src(fonts)
    .pipe(gulp.dest('./public/fonts/'));   
});

gulp.task('watch', function() {
    gulp.watch(js, function(){
      gulp.run('lint', 'minify-js');
    });
    
    gulp.watch(css, ['minify-css']);
});

gulp.task('start-project', function() {
  console.log(Date(), 'start-project called');
  
  return gulp.src('src/**/*.js')
             .pipe(shell('npm start'));
});

gulp.task('default', defaultTasks);
