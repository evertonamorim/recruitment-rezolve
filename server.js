var forever = require('forever-monitor');
var Monitor = forever.Monitor;

var child = new Monitor('app.js', {
  max: 10,
  silent: false,
  killTree: true,
  logFile: 'logs/forever.log',
  outFile: 'logs/app.log',
  errFile: 'logs/error.log'
});

child.on('exit', function () {
  console.log('The server has started. ');
});

child.start();
